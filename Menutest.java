package guan.amc;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

/**
 * options menu是和Activity相关联的 ,ontext menu 是和View关联。 context
 * menu只能通过setOnCreateContextMenuListener来进行关联。
 * 
 * @author Administrator
 * 
 */
public class test extends Activity
{
	/** Called when the activity is first created. */
	Button button;

	@Override
	public void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
		button = (Button) findViewById(R.id.button);
		button.setOnCreateContextMenuListener(this);
	}

	public boolean onCreateOptionsMenu(Menu menu)
	{

		menu.add(0, 0, 0, "about");
		menu.add(0, 1, 1, "quit");
		return super.onCreateOptionsMenu(menu);
	}

	public boolean onOptionsItemSelected(MenuItem item)
	{
		super.onOptionsItemSelected(item);
		switch (item.getItemId())
		{
		case 0:
			Toast.makeText(test.this, "hello", Toast.LENGTH_LONG).show();
			return true;
		case 1:
			this.finish();
		}
		return true;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View view,
			ContextMenuInfo menuInfo)
	{
		menu.setHeaderTitle("颜色");
		menu.add(0, 0, 0, "红色");
		menu.add(0, 1, 0, "蓝色");
	}

	@Override
	public boolean onContextItemSelected(MenuItem item)
	{
		button.setText(item.getTitle());
		switch (item.getItemId())
		{
		case 0:
			button.setBackgroundColor(Color.RED);
			break;
		case 1:
			button.setBackgroundColor(Color.BLUE);
			break;
		}
		return false;

	}
}